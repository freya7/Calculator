package dtu.compute;

import io.cucumber.java.en.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StepDefinitions {
  private String input;

  @When("adding {string}")
  public void adding(String s) {
    input = s.replace("\\n","\n");
  }

  @Then("result in {int}")
  public void result_in(int i) {
    StringCalculator sc = new StringCalculator();
    assertEquals(i,sc.add(input));
  }

  @Then("throw an error {string}")
  public void throwAnError(String exceptionMessage) {
    StringCalculator sc = new StringCalculator();
    Exception e = assertThrows(Exception.class, () -> sc.add(input));
    assertEquals(exceptionMessage, e.getMessage());
  }
}