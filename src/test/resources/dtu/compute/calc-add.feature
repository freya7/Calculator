Feature: Calculator Addition

  Scenario: Add 0 numbers
    When adding ""
    Then result in 0

  Scenario: Add 1 number
    When adding "1"
    Then result in 1

  Scenario: Add 2 numbers
    When adding "2,3"
    Then result in 5

  Scenario: Add 4 numbers
    When adding "1,2,3,4"
    Then result in 10

  Scenario: Add 2 numbers with different delimiter
    When adding "//;\n1;2"
    Then result in 3

  Scenario: Add 4 numbers with different delimiter
    When adding "//;\n1;2;3;4"
    Then result in 10

  Scenario: Add negative number
    When adding "-1,2"
    Then throw an error "negatives not allowed: [-1]"

  Scenario: Add negative numbers
    When adding "-1,2,-3"
    Then throw an error "negatives not allowed: [-1, -3]"
