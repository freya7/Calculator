package dtu.compute;

import java.util.ArrayList;
import java.util.List;

public class StringCalculator {
  public StringCalculator(){}

  public int add(String args){
    // Zero values given
    if (args.equals("")){
      return 0;
    }

    // Values given
    int result = 0;
    List<Integer> negativeValues = new ArrayList<>();
    String delimiter;
    String elements;

    // If delimiter change
    if (args.startsWith("//")){
      delimiter = args.substring(2,3);
      String[] inputArray = args.split(System.getProperty("line.separator"));
      elements = inputArray[1];
    } else {
      delimiter = ",";
      elements = args;
    }

    // Go through elements
    String[] argArray = elements.split(delimiter);
    for (String s : argArray){
      int v = Integer.parseInt(s);
      if (v < 0) {
        negativeValues.add(v);
      } else {
        result += v;
      }
    }

    if (negativeValues.size() > 0){
      throw new ArithmeticException("negatives not allowed: " + negativeValues.toString());
    }
    return result;
  }
}
